const search = require('./search')
const template = require('./template')

module.exports = middleware

function middleware (options) {
  return function (req, res) {
    var year = parseInt(req.query.year)
    var keywords = (req.query.q || '').trim().split(' ')
    search.run(options.basedir, year, keywords)
      .then((hits) => {
        res.format({
          html: function () {
            res.send(template(req, hits))
          },
          json: function () {
            res.send(hits)
          }
        })
      })
      .catch((err) => {
        console.error(err)
      })
  }
}
