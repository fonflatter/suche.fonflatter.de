const _ = require('lodash')
const fs = require('fs-extra')
const textSearch = require('rx-text-search')

module.exports = {
  run: runSearch
}

function runSearch (basedir, year, keywords) {
  if (!year || !keywords || !keywords[0]) {
    return Promise.resolve([])
  }

  return textSearch.findAsPromise(keywords, year + '/**/*.json', {cwd: basedir})
    .then(function (searchResult) {
      var numMatchesPerFile = _.countBy(searchResult, 'file')
      var fileNames = Object.keys(numMatchesPerFile).sort().reverse()
      var hits = fileNames.map((fileName) => {
        return {
          fileName: fileName,
          comicDate: fileName.substr(0, 10),
          numMatches: numMatchesPerFile[fileName]
        }
      })
      hits = _.uniqBy(hits, 'comicDate')
      hits = _.sortBy(hits, ['numMatches', 'fileName']).reverse()
      hits = _.map(hits, (hit) => {
        var transcription = fs.readJsonSync(basedir + hit.fileName)
        return {
          comicUrl: 'https://www.fonflatter.de/' + hit.comicDate + '/',
          comicDate: hit.comicDate.split(/\//).reverse().join('.'),
          text: transcription.text
        }
      })
      return hits
    })
}
