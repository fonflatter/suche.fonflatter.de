const config = require('./config')
const express = require('express')
const morgan = require('morgan')
const searchMiddleware = require('./middleware')

const app = express()
app.use(morgan(config.logformat))
app.get('/', searchMiddleware(config))

module.exports = app
