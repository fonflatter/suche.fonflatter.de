const _ = require('lodash')

module.exports = function (req, hits) {
  var years = _.range(2005, new Date().getFullYear() + 1).reverse()
  var html = `
<form method="get">
    <input name="q" type="text" value="${_.escape(req.query.q) || ''}">
    <select name="year">
      ${years.map((year) => `<option ${year === parseInt(req.query.year) ? 'selected' : ''}>${year}</option>`)}
    </select>
    <input type="submit" value="suchen">
</form>
    `

  if (req.query.q && req.query.year) {
    switch (hits.length) {
      case 0:
        html += '<p>Keine Ergebnisse!</p>'
        break
      case 1:
        html += '<p>Nur ein einziges Ergebnis...</p>'
        break
      default:
        html += `<p>${hits.length} Ergebnisse</p>`
        break
    }
  }

  html += '<dl>'
  hits.forEach((hit) => {
    html += `<dt><a href="${hit.comicUrl}">${hit.comicDate}</a></dt>`
    html += `<dd>${hit.text.split(/\r?\n/).join('<br>\n')}</dd>`
  })
  html += '</dl>'

  return html
}
